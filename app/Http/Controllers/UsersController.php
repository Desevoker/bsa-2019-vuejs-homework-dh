<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserResource::collection(User::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->validate([
            'name' => ['required', 'string', 'between:3,255'],
            'email' => ['required', 'email', 'unique:users'],
            'avatar' => ['nullable', 'url', 'max:255'],
            'password' => ['required', 'string', 'between:6,255'],
        ]);

        $attributes['password'] = bcrypt($attributes['password']);

        $user = User::create($attributes);

        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if (! $user) {
            return new JsonResponse([
                'message' => "User with id {$id} not found."
            ], 404);
        }

        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'between:3,255'],
            'email' => ["required", 'email'],
            'avatar' => ['nullable', 'url', 'max:255'],
        ]);

        $validator->sometimes('email', 'unique:users', function ($input) use ($user) {
            return $input->email !== $user->email;
        });

        $attributes = $validator->validated();

        $user->fill($attributes)->save();

        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (! $user) {
            return new JsonResponse([
                'message' => "User with id {$id} not found."
            ], 404);
        }

        $user->delete();

        return new JsonResponse(null, 204);
    }
}
